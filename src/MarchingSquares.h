#ifndef _MARCHING_SQUARES_H_
#define _MARCHING_SQUARES_H_

class Renderable;

namespace MarchingSquares {

    /**
     * @param img 2D array of square input data
     * @return Renderable marching squares output
     */
    Renderable* march( float *img, unsigned w );

    extern float threshold;
}

#endif