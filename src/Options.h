#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include <string>

namespace Options {
    /**
     * Toggles the value of a boolean.
     * @param b bool to toggle
     * @return the new (post-toggle) value of b
     */
    bool toggle( bool& b );

    /**
     * Parses command-line options.
     * Processed arguments are removed from argc and argv.
     * @param argc will be mutated in-place
     * @param argv will be mutated in-place
     */
    void parse( int &argc, char **argv );

    extern unsigned width;
    extern unsigned height;

    namespace Display {
        extern bool Wireframe;
        extern bool UseDisplayLists;
    }

    namespace MarchingSquares {
        extern float Blur;
        extern bool Interpolate;
    }

    template <typename T>
    class Option {
    public:

        /**
         * @param shortflag like "b" to match argument "-b"
         * @param desc
         * @param pDstArg pointer to object that will received parsed argument
         */
        Option( const std::string &shortflag, const std::string &desc, T* pDstArg ) throw()
            : m_flag( shortflag ), m_desc( desc ), m_pDstArg( pDstArg )
        { }

        virtual ~Option()
        { }

        /**
         * Consumes flag and one argument.
         * @return m_pDstArg
         */
        T* consume( char **args ) const
        {
            if( sscanf( args[1], formatString(), m_pDstArg ) == 1 ) {
            }
            else {
                throw new std::exception( "Unable to parse argument" );
            }
            return m_pDstArg;
        }

        const char* formatString() const
        { throw new std::logic_error( "Not implemented" ); }

    private:
        T* m_pDstArg;
        const std::string m_desc;
        const std::string m_flag;
    };
    template<> const char* Option<float>::formatString() const
    { return "%f"; }
};

#endif
