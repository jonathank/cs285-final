#include "Options.h"
#include <cstring>
#include <stdio.h>

unsigned Options::width = 800;
unsigned Options::height = 600;

bool Options::Display::Wireframe = true;
bool Options::Display::UseDisplayLists = false;

float Options::MarchingSquares::Blur = 1.0f;
bool Options::MarchingSquares::Interpolate = true;

bool Options::toggle( bool &b ) {
    return (b = !b);
}

void Options::parse( int &argc, char **argv ) {
    char **pDstArg = argv;
    char **pCurArg = argv;
    int iArg = 0;

    while( iArg <= argc+1 ) {
        char *arg = *pCurArg;

        /* -b %f    [ blur ] */
        if( !strcmp( arg, "-b" ) ) {
            Options::Option<float>( "-b", "blur", &Options::MarchingSquares::Blur ).consume( pCurArg );
            printf( "Set blur to %f\n", Options::MarchingSquares::Blur );
            pCurArg++; /* consumed an extra arg */
            ++iArg;
            argc -= 2;
        }

        else {
            *pDstArg = arg;
            ++pDstArg;
        }

        pCurArg++;
        ++iArg;
    }
}

