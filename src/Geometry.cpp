#include "common.h"
#include "Geometry.h"
#include "Options.h"
#include <gl/glut.h>
#include <stdarg.h>
#include <ostream>

Renderable::Renderable()
    : m_pos( 0, 0, 0 ), m_rot( 0, 0, 0 ), m_scale( 1, 1, 1 ), m_refCount( 0 )
{
}

Renderable::~Renderable() {
    for_each_renderable( *this, it ) {
        (**it).relClaim();
    }
}

void Renderable::beginDraw( unsigned mode ) {
    if( Options::Display::Wireframe )
        glBegin( GL_LINE_LOOP );
    else
        glBegin( mode );
}

void Renderable::render() {
    _preRender();
    _render();
    for_each_renderable( *this, it ) {
        (**it).render();
    }
    _postRender();
}

void Renderable::_preRender() {
    _applyTransforms( /* fInverse = */ false );
}

void Renderable::_postRender() {
    _applyTransforms( /* fInverse = */ true );
}

void Renderable::_render() {
}

void Renderable::_applyTransforms( bool fInverse /* = false */ )
{
    if( fInverse ) {
        glPopMatrix();
    } else {
        glPushMatrix();
        glScalef( m_scale.x, m_scale.y, m_scale.z );
        glRotatef( m_rot.x, World::X.x, World::X.y, World::X.z );
        glRotatef( m_rot.y, World::Y.x, World::Y.y, World::Y.z );
        glRotatef( m_rot.z, World::Z.x, World::Z.y, World::Z.z );
        glTranslated( m_pos.x, m_pos.y, m_pos.z );
    }
}

bool Renderable::fAddChild( const Renderable* child ) {
    children().push_back( const_cast< Renderable* >( child ) );
    const_cast< Renderable* >( child )->addClaim();
    return true;
}

void Renderable::clear( bool fGc /* = true */ ) {
    for_each_renderable( *this, it ) {
        Renderable *child = *it;
        child->relClaim();
        if( fGc )
            child->gc();
    }
    m_children.clear();
}

void Renderable::scale( nv_scalar s ) {
    m_scale = s * m_scale;
}

void Renderable::gc( bool fRecurse /* = true */ ) {
    if( fRecurse ) {
        for_each_renderable( *this, it )
            (**it).gc( true );
    }

    if( m_refCount == 0 )
        delete this;
}

/***********************************************
  Vertex
************************************************/
void Vertex::_render() {
    glVertex3f( x, y, z );
    Renderable::_render();
}

void Vertex::saveSTL( std::ostream& out ) const {
    out << "vertex "
        << x << " "
        << y << " "
        << z << std::endl;
}

/***********************************************
  Face
************************************************/
Face::Face() throw()
    : Renderable()
{
}

Face::Face( const Vertex* a, const Vertex* b, const Vertex* c, const Vertex* d /* = nullptr */ )
    : Renderable()
{
    fAddChild( a );
    fAddChild( b );
    fAddChild( c );
    if( d )
        fAddChild( d );
}

Face::Face( unsigned cVerts, ... )
    : Renderable()
{
    va_list args;
    va_start( args, cVerts );

    for( unsigned iVert = 0; iVert < cVerts; ++iVert ) {
        fAddChild( va_arg( args, Vertex* ) );
    }

    va_end( args );
}

void Face::_preRender() {
    Renderable::_preRender();
    beginDraw( GL_POLYGON );
}

void Face::_postRender() {
    glEnd();
    Renderable::_postRender();
}

const vec3 Face::normal() const {
    vec3 n( vec3_null );
    vec3 a( *getVertex( 1 ) - *getVertex( 0 ) );
    vec3 b( *getVertex( 2 ) - *getVertex( 0 ) );
    cross( n, a, b );
    return n;
}

void Face::saveSTL( std::ostream& out ) const {
    const vec3 n = normal();
    const unsigned cVerts = num_vertices();

    /* Tesselate
       Vertex 0 is always the start point.
       For each consecutive pair, make a triangle with Vertex 0.
    */

    /* TODO refactor this */
    const Vertex *a = getVertex( 0 );
    for( unsigned i = 1; i < cVerts - 1; ++i ) {
        const Vertex *b = getVertex( i );
        const Vertex *c = getVertex( i+1 );

        out << "facet normal "
            << n.x << " "
            << n.y << " "
            << n.z << std::endl;
        out << "outer loop" << std::endl;
        a->saveSTL( out );
        b->saveSTL( out );
        c->saveSTL( out );
        out << "endloop" << std::endl;
        out << "endfacet" << std::endl;
    }
}

/***********************************************
  Mesh
************************************************/

bool Mesh::fAddFace( Face *f ) {
    m_faces.push_back( f );
    return true;
}

bool Mesh::fAddVertex( Vertex *v ) {
    m_vertices.push_back( v );
    return true;
}

void Mesh::_render() {
    Renderable::_render();
    for_each_( std::vector< Face* >, m_faces, it ) {
        (**it).render();
    }
}

/***********************************************
  Plane
************************************************/

Plane::Plane( unsigned subdivisions, bool fCentered )
    : m_subdivisions( subdivisions ), Mesh()
{
    unsigned n = m_subdivisions + 1;

    const nv_scalar d = 1.0 / subdivisions;
    const nv_scalar offset = ( fCentered ? -0.5 : 0.0 );

    for( unsigned row = 0; row < n; ++row ) {
        for( unsigned col = 0; col < n; ++col ) {
            Vertex *v = new Vertex( col * d + offset, row * d + offset, 0.0 );
            fAddVertex( v );
        }
    }

    for( unsigned row = 0; row < n-1; ++row ) {
        for( unsigned col = 0; col < n-1; ++col ) {
            Vertex *a = vertices()[  row   *n + col   ];
            Vertex *b = vertices()[  row   *n + col+1 ];
            Vertex *c = vertices()[ (row+1)*n + col+1 ];
            Vertex *d = vertices()[ (row+1)*n + col   ];
            Face *f = new Face( a, b, c, d );
            fAddFace( f );
        }
    }
}

Plane::~Plane() {
}

Vertex* Plane::getVertexByIndex( unsigned iX, unsigned iY )
{
    if( iX > m_subdivisions || iY > m_subdivisions ) {
        throw new std::invalid_argument( "Vertex index out of bounds" );
    }

    return vertices()[ iY*(m_subdivisions+1) + iX ];
}

