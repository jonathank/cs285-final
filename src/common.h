#ifndef _COMMON_H_
#define _COMMON_H_

#include <memory>
#include <assert.h>

#define for_each_( T, collection, it ) \
    for( T##::iterator it = collection.begin(); it != collection.end(); it++ )

#endif
