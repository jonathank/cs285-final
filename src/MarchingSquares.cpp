#include "MarchingSquares.h"
#include "Geometry.h"
#include "common.h"
#include "Options.h"

typedef unsigned char bits;

float MarchingSquares::threshold = 0.5f;

std::vector< Vertex* > g_interpVerts;
std::vector< Face* > *g_p_faces;
unsigned g_w = 0;

vec3 lerp( const vec3& a, const vec3& b, float fa, float fb ) {
    if( fa > fb )
        return lerp( b, a, fb, fa );

    nv_scalar x = 0.5;

    if( Options::MarchingSquares::Interpolate
        && fa < MarchingSquares::threshold      /* Can only interpolate if   */
        && fb >= MarchingSquares::threshold     /* we cross the threshold    */
        ) {
            x = (MarchingSquares::threshold - fa) / (fb - fa);
            x = 1.0 - x;
    }

    vec3 v = x*a + (1.0-x)*b;

    return v;
}

typedef enum {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
} Side;

Vertex* getInterpVert( unsigned row, unsigned col, Side side, float a, float b, float c, float d ) {
    /*

       Each cell has four interpolated vertices: LEFT, RIGHT, TOP, BOTTOM

       For an n-by-n grid, there are n TOPs, n BOTTOMs,
       n+1 LEFTs, and n+1 RIGHTs per row.

       Since each row has n tops and n+1 sides, the index of the top vert
       for row i is i*(w + w+1). Left is w+1 after that, and bottom corresponds
       to the next row.

          +---0---+---1---+---2---+
          |       |       |       |
          3 (0,0) 4 (0,1) 5 (0,2) 6       w = 3
          |       |       |       |
          +---7---+---8---+---9---+
          |       |       |       |
         10 (1,0) 11(1,1) 12(1,2) 13
          |       |       |       |
          +--14---+--15---+--16---+

    */

    unsigned w = g_w;

    if( side == BOTTOM )
        ++row;

    unsigned iVert = row * (w + w+1) + col;  // top
    if( side & LEFT | RIGHT )
        iVert += w;
    if( side & RIGHT )
        ++iVert;

    if( !g_interpVerts[ iVert ] ) {
        // Need to compute this vert
        const Face& f = *(g_p_faces->at( row*w + col ));
        const Vertex *topLeft     = f.getVertex( 3 );
        const Vertex *topRight    = f.getVertex( 2 );
        const Vertex *bottomRight = f.getVertex( 1 );
        const Vertex *bottomLeft  = f.getVertex( 0 );

        switch( side ) {
        case LEFT:
            g_interpVerts[ iVert ] = new Vertex( lerp( *topLeft, *bottomLeft, d, a ) );
            break;
        case RIGHT:
            g_interpVerts[ iVert ] = new Vertex( lerp( *topRight, *bottomRight, c, b ) );
            break;
        case TOP:
            g_interpVerts[ iVert ] = new Vertex( lerp( *topLeft, *topRight, d, c ) );
            break;
        case BOTTOM:
            g_interpVerts[ iVert ] = new Vertex( lerp( *bottomLeft, *bottomRight, a, b ) );
            break;
        default:
            throw new std::logic_error( "Invalid side" );
            break;
        }
    }

    return g_interpVerts[ iVert ];
}

typedef enum {
    LEFT_BOTTOM  = 0x1,
    RIGHT_BOTTOM = 0x2,
    LEFT_TOP     = 0x4,
    RIGHT_TOP    = 0x8,
    HORIZONTAL   = 0x10,
    VERTICAL     = 0x20
} SplitFlags;

Renderable* processCell( unsigned row, unsigned col, float a, float b, float c, float d )
{
    Renderable *newFace = nullptr;
    const Face& oldFace = *(g_p_faces->at( row*g_w + col ));

    /* true = split here */
    unsigned split = 0x0;
    bool flip = false;

    bits bitz = 0;
    bitz |= (a >= MarchingSquares::threshold ? 1 : 0);
    bitz |= (b >= MarchingSquares::threshold ? 1 : 0) << 1;
    bitz |= (c >= MarchingSquares::threshold ? 1 : 0) << 2;
    bitz |= (d >= MarchingSquares::threshold ? 1 : 0) << 3;

    switch( bitz )
    {
    case 0:
        return nullptr;
        break;

    case 1:
        split = LEFT_BOTTOM;
        break;

    case 2:
        split = RIGHT_BOTTOM;
        break;

    case 3:
        split = HORIZONTAL;
        break;

    case 4:
        split = RIGHT_TOP;
        break;

    case 5:
        split = LEFT_TOP | RIGHT_BOTTOM;
        break;

    case 6:
        split = VERTICAL;
        flip = true;
        break;

    case 7:
        split = LEFT_TOP;
        flip = true;
        break;

    case 8:
        split = LEFT_TOP;
        break;

    case 9:
        split = VERTICAL;
        break;

    case 10:
        split = LEFT_BOTTOM | RIGHT_TOP;
        break;

    case 11:
        split = RIGHT_TOP;
        flip = true;
        break;

    case 12:
        split = HORIZONTAL;
        flip = true;
        break;

    case 13:
        split = RIGHT_BOTTOM;
        flip = true;
        break;

    case 14:
        split = LEFT_BOTTOM;
        flip = true;
        break;

    case 15:
        flip = true;
        break;

    default:
        throw new std::invalid_argument( "Invalid marching squares index" );
        break;
    }

    newFace = new Group();

    const Vertex *topLeft     = oldFace.getVertex( 0 );
    const Vertex *topRight    = oldFace.getVertex( 1 );
    const Vertex *bottomRight = oldFace.getVertex( 2 );
    const Vertex *bottomLeft  = oldFace.getVertex( 3 );

    Vertex *left   = new Vertex( lerp( *topLeft, *bottomLeft, d, a ) );
    Vertex *right  = new Vertex( lerp( *topRight, *bottomRight, c, b ) );
    Vertex *top    = new Vertex( lerp( *topLeft, *topRight, d, c ) );
    Vertex *bottom = new Vertex( lerp( *bottomLeft, *bottomRight, a, b ) );

    //Vertex *left   = getInterpVert( row, col, LEFT,   a, b, c, d );
    //Vertex *right  = getInterpVert( row, col, RIGHT,  a, b, c, d );
    //Vertex *top    = getInterpVert( row, col, TOP,    a, b, c, d );
    //Vertex *bottom = getInterpVert( row, col, BOTTOM, a, b, c, d );

    if( (split & LEFT_BOTTOM) && (split & RIGHT_TOP) ) {
        newFace->fAddChild( new Face( 6, top, topLeft, left, bottom, bottomRight, right ) );
    }
    else if( split & LEFT_BOTTOM ) {
        if( flip )
            newFace->fAddChild( new Face( 5, left, topLeft, topRight, bottomRight, bottom ) );
        else
            newFace->fAddChild( new Face( bottomLeft, bottom, left ) );
    }
    else if( split & RIGHT_TOP ) {
        if( flip )
            newFace->fAddChild( new Face( 5, top, topLeft, bottomLeft, bottomRight, right ) );
        else
            newFace->fAddChild( new Face( topRight, top, right ) );
    }

    if( (split & RIGHT_BOTTOM) && (split & LEFT_TOP) ) {
        newFace->fAddChild( new Face( 6, topRight, top, left, bottomLeft, bottom, right ) );
    }
    else if( split & RIGHT_BOTTOM ) {
        if( flip )
            newFace->fAddChild( new Face( 5, bottom, right, topRight, topLeft, bottomLeft ) );
        else
            newFace->fAddChild( new Face( bottomRight, right, bottom ) );
    }
    else if( split & LEFT_TOP ) {
        if( flip )
            newFace->fAddChild( new Face( 5, top, left, bottomLeft, bottomRight, topRight ) );
        else
            newFace->fAddChild( new Face( topLeft, left, top ) );
    }

    if( split & VERTICAL ) {
        if( flip )
            newFace->fAddChild( new Face( topRight, top, bottom, bottomRight ) );
        else
            newFace->fAddChild( new Face( topLeft, bottomLeft, bottom, top ) );
    }
    if( split & HORIZONTAL ) {
        if( flip )
            newFace->fAddChild( new Face( topLeft, left, right, topRight ) );
        else
            newFace->fAddChild( new Face( bottomLeft, bottomRight, right, left ) );
    }
    if( split == 0 ) {
        if( flip )
            newFace->fAddChild( new Face( topLeft, bottomLeft, bottomRight, topRight ) );
    }

    /* Garbage collect */
    left->gc();
    right->gc();
    top->gc();
    bottom->gc();

    return newFace;
}

Renderable* MarchingSquares::march( float *img, unsigned w )
{
    g_w = w;
    Group *g = new Group();
    Plane *r = new Plane( w );
    //g->fAddChild( r );
    g_p_faces = &r->faces();

    g_interpVerts.assign( w*(w + w+1) + w, nullptr );

    for( unsigned row = 0; row < w-1; ++row ) {
        for( unsigned col = 0; col < w-1; ++col ) {
            Face *f = r->faces()[ row*w + col ];

            /*
                http://en.wikipedia.org/wiki/Marching_squares

                Cells are processed according to
                
                    8d --- 4c
                    |       |
                    |       |
                    1a --- 2b
            */

            float d = img[  row   *w + col   ];
            float c = img[  row   *w + col+1 ];
            float b = img[ (row+1)*w + col+1 ];
            float a = img[ (row+1)*w + col   ];

            Renderable *newFace = processCell( row, col, a, b, c, d );
            if( newFace )
                g->fAddChild( newFace );
        }
    }

    return g;
}

