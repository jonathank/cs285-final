#include <stdlib.h>
#include "GLSetup.h"
#include "Scene.h"
#include "Geometry.h"
#include "Options.h"

typedef float Color;

void printHelp() {
    printf("Usage: final.exe <input.bmp> [<output.stl>]\n");
}

int main( int argc, char* argv[] ) {
    glutInit( &argc, argv );
    Options::parse( argc, argv );
    GL::setup();

    //printf("argc is %d and argv is\n", argc );
    //for( unsigned iArg = 0; iArg < argc; ++iArg )
    //    printf("\t%s\n", argv[iArg] );

    if( argc < 2 ) {
        printHelp();
        exit( 0 );
    }
    else {
        /* Arg 1: filename */
        std::string filename( argv[1] );
        g_scene->load( filename );

        /* Arg 2: output */
        if( argc >= 3 ) {
            saveSTL( argv[2] );
            exit( 0 );
        } else {
            /* No output specified; render OpenGL */
            glutMainLoop();
        }
    }

    //CImgDisplay main_disp(image,"Click a point"), draw_disp(visu,"Intensity profile");

    glutMainLoop();

    //while (!main_disp.is_closed() && !draw_disp.is_closed()) {

    //    main_disp.wait();

    //    cimg_forXYZC( image, x, y, z, k ) {
    //        Color c = image( x, y, z, k );
    //        visu( x, y, z, k ) = 255 - c;
    //    }
    //    visu.display("ololo");
    //    printf("ok\n");

    //    //if (main_disp.button() && main_disp.mouse_y()>=0) {
    //    //    const int y = main_disp.mouse_y();
    //    //    visu.fill(0).draw_graph(image.get_crop(0,y,0,0,image.width()-1,y,0,0),red,1,1,0,255,0);
    //    //    visu.draw_graph(image.get_crop(0,y,0,1,image.width()-1,y,0,1),green,1,1,0,255,0);
    //    //    visu.draw_graph(image.get_crop(0,y,0,2,image.width()-1,y,0,2),blue,1,1,0,255,0).display(draw_disp);
    //    //}

    //}
    return 0;
}