#ifndef _SCENE_H_
#define _SCENE_H_

#include <memory>
#include <string>

class Renderable;

class Scene {
public:
    Scene() throw();
    virtual ~Scene();

    /**
     * @return root node of the scene
     */
    Renderable* root() throw();

    Renderable* camera() throw();

    void render();

    /**
     * Regenerate the scene from scratch.
     */
    void rebuild();

    /**
     * Read the specified file and rebuild.
     * @param file scene file to load
     */
    void load( const std::string &file );

private:
    std::shared_ptr< Renderable > m_root;
    std::shared_ptr< Renderable > m_camera;
    std::string m_file;
};

extern Scene* g_scene;
extern void setupInitialScene();

#endif
