#include "Scene.h"
#include "Geometry.h"
#include "Options.h"
#include "GLSetup.h"
#include "MarchingSquares.h"
#include "CImg.h"
using namespace cimg_library;

Scene* g_scene = new Scene();
GLuint *iSceneList = nullptr;

Scene::Scene() throw()
    : m_file( "" )
{
    m_root.reset< Renderable >( new Renderable() );
    m_camera.reset< Renderable >( new Renderable() );

    m_camera->pos() = 5.0 * World::Z;
}

Scene::~Scene() {
    if( iSceneList ) {
        glDeleteLists( *iSceneList, 1 );
        delete iSceneList;
    }
}

Renderable* Scene::root() throw()
{
    return m_root.get();
}

Renderable* Scene::camera() throw() {
    return m_camera.get();
}

void Scene::render()
{
    if( Options::Display::UseDisplayLists ) {
        if( !iSceneList ) {
            iSceneList = new GLuint( glGenLists( 1 ) );
            glNewList( *iSceneList, GL_COMPILE );
            root()->render();
            glEndList();
        }
        glCallList( *iSceneList );
    } else {
        root()->render();
    }
}

void Scene::rebuild() {
    CImg< float > image( m_file.c_str() ); //, visu(1000,1000,1,3,0);
    //const float red[] = { 255,0,0 }, green[] = { 0,255,0 }, blue[] = { 0,0,255 };

    //image.blur(2.5);
    image.blur(Options::MarchingSquares::Blur);

    float *grey = new float[ image.width() * image.height() ];
    cimg_forXY( image, x, y ) {
        float r = image( x, y, 0 );
        float g = image( x, y, 1 );
        float b = image( x, y, 2 );
        float lum = 0.30*r + 0.59*b + 0.11*g;
        lum /= 255.0f;
        lum = 1.0f - lum;
        grey[ x*image.width() + y ] = lum;
    }

    Renderable *r = MarchingSquares::march( grey, image.width() );
    delete grey;

    r->scale( 25.0 );

    root()->clear( /* fGc = */ true );
    root()->fAddChild( r );
}

void Scene::load( const std::string& file ) {
    m_file = file;
    printf( "Loading '%s'...\n", file.c_str() );
    rebuild();
    printf( "Done.\n" );
}

void setupInitialScene() {
    //Mesh *m = new Mesh();

    //Vertex *a = new Vertex( 0, 0, 0 );
    //Vertex *b = new Vertex( 1, 0, 0 );
    //Vertex *c = new Vertex( 1, 1, 0 );
    //Vertex *d = new Vertex( 0, 1, 0 );
    //Face *f1 = new Face( a, b, c, d );
    //m->fAddFace( f1 );

    //g_scene->root()->fAddChild( m );

    Plane *p = new Plane( 5 );
    p->scale( 5.0 );
    g_scene->root()->fAddChild( p );
}
