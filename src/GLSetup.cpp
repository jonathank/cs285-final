#include "GLSetup.h"
#include <nv_math.h>
#include "Options.h"
#include "Scene.h"
#include "Geometry.h"
#include "MarchingSquares.h"
#include <stdio.h>
#include <fstream>

void GL::registerCallbacks() {

    glutDisplayFunc( GL::display );
    glutKeyboardFunc( GL::keyboard );
    glutSpecialFunc( GL::specialKey );
    glutMouseFunc( GL::mouse );
    glutReshapeFunc( GL::reshape );
    glutPassiveMotionFunc( GL::passiveMotion );
    glutMotionFunc( GL::drag );

}

void GL::setup() {
    glutInitWindowSize( 800, 600 );   // TODO: refactor this
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutCreateWindow( "CS285 Final" );

    registerCallbacks();
}

void GL::display() {
    glClearColor(1, 1, 1, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable( GL_LIGHTING );
    glEnable(GL_DEPTH_TEST);

    {
        GLfloat light_specular[] = {1, 0.5, 0, 1};
        glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
        glEnable(GL_LIGHT0);
    }

    {
        GLfloat one[] = {1, 1, 1, 1};
        GLfloat small[] = {0.2, 0.2, 0.2, 1};
        GLfloat high[] = {100};
        glMaterialfv(GL_FRONT, GL_AMBIENT, one);
        glMaterialfv(GL_FRONT, GL_SPECULAR, one);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, small);
        glMaterialfv(GL_FRONT, GL_SHININESS, high);
    }

    vec3 eye( 0, 0, 5 );
    vec3 center( 0, 0, 0 );
    vec3 up( 0, 1, 0 );
    gluLookAt(
        eye.x, eye.y, eye.z,
        center.x, center.y, center.z,
        up.x, up.y, up.z
        );

    //Second, the framebuffer is filled with the objects in the scene
    if( g_scene ) {
        vec3 eye = g_scene->camera()->pos();
        gluLookAt(
            eye.x, eye.y, eye.z,
            0.0, 0.0, 0.0, // TODO: center
            World::UP.x, World::UP.y, World::UP.z // TODO: Renderable local coords
            );

        g_scene->render();
    }
    glutSwapBuffers();
}

void GL::keyboard( unsigned char key, int x, int y ) {
    switch( key )
    {
    case GL::Keys::ESC:
        exit( 0 );
        break;
    default:
        printf( "keypress %u\n", (unsigned) key );
        return;   /* Do not redisplay */
        break;
    }

    glutPostRedisplay();
}

void GL::specialKey( int key, int x, int y ) {
    /* Used for camera movement */
    const vec3 rg_dpos[] = { -World::X, -World::Z, World::X, World::Z };
    const vec3 *p_dpos = nullptr;

    switch( key )
    {
    /* Camera movement */
    case GL::Keys::KP_LEFT:
    case GL::Keys::KP_UP:
    case GL::Keys::KP_RIGHT:
    case GL::Keys::KP_DOWN:
        p_dpos = &rg_dpos[ key - GL::Keys::KP_LEFT ];
        g_scene->camera()->pos() += 0.5 * *p_dpos;
        break;

    /* Wireframe */
    case GL::Keys::F3:
        Options::toggle( Options::Display::Wireframe );
        break;

    /* Interpolate */
    case GL::Keys::F4:
        Options::toggle( Options::MarchingSquares::Interpolate );
        g_scene->rebuild();
        break;

        /* Save to STL */
    case GL::Keys::F9:
        saveSTL( "output.stl" );
        break;

    default:
        printf( "specialkey %u\n", (unsigned) key );
        return;  /* Do not redisplay */
        break;
    }

    glutPostRedisplay();
}

void GL::reshape( int w, int h ) {
    Options::width = w;
    Options::height = h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective( 90, (double) w / (double) h, 0.0001, 99 );
    glViewport( 0, 0, w, h );

    glutPostRedisplay();
}

void GL::passiveMotion( int x, int y ) {
}

void GL::drag( int x, int y ) {
}

void GL::mouse( int button, int state, int x, int y ) {
}

void saveSTL( const std::string& filename )
{
    printf( "Writing to %s...\n", filename.c_str() );
    std::ofstream out( filename.c_str(), std::ios_base::trunc | std::ios_base::out );
    out << "solid output" << std::endl;
    g_scene->root()->saveSTL( out );
    out << "endsolid output" << std::endl;
    printf( "Done.\n" );
}