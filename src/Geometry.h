#ifndef _GEOMETRY_H_
#define _GEOMETRY_H_

#include <vector>
#include <list>
#include <nv_math.h>

#define for_each_renderable( parent, it ) \
    for( Renderable::RenderableList::iterator it = (parent).children().begin(); it != (parent).children().end(); it ++ )
#define for_each_renderable_const( parent, it ) \
    for( Renderable::RenderableList::const_iterator it = (parent).children().begin(); it != (parent).children().end(); it ++ )

namespace World {
    const vec3 X( 1.0, 0.0, 0.0 );
    const vec3 Y( 0.0, 1.0, 0.0 );
    const vec3 Z( 0.0, 0.0, 1.0 );
    const vec3 UP = Y;
}

/**
* An abstract OpenGL-renderable item.
*/
class Renderable
{
public:

    typedef std::vector<Renderable*> RenderableList;

    Renderable() throw();
    virtual ~Renderable() throw();

    /**
    * Render this object recursively.
    * Subclasses should *not* override this method,
    * but instead use _render.
    * Order of operations is
    * 1. _preRender
    * 2. _render
    * 3. render() children
    * 4. _postRender
    */
    void render();

    /**
     * @return direct children of this Renderable
     */
    RenderableList& children() throw()
    { return m_children; }

    const RenderableList& children() const throw()
    { return m_children; }

    /**
     * Adds a new child to this Renderable.
     * @param child
     */
    bool fAddChild( const Renderable* child );

    /**
     * @return ith child
     */
    const Renderable* getChild( unsigned iChild ) const
      { return m_children[ iChild ]; }
    Renderable* getChild( unsigned iChild )      
      { return m_children[ iChild ]; }

    /**
     * Disown all children.
     * @param fGc garbage collect children?
     */
    void clear( bool fGc = true );

    const vec3& pos() const { return m_pos; }
          vec3& pos()       { return m_pos; }

    const vec3& rot() const { return m_rot; }
          vec3& rot()       { return m_rot; }

    const vec3& scale() const { return m_scale; }
          vec3& scale()       { return m_scale; }

    /**
     * Scales uniformly
     * @param s scale factor
     */
    void scale( nv_scalar s );

    /**
     * Perform garbage collection on this Renderable.
     * Delets this object if m_refCount == 0.
     * @param fRecurse recurse on children?
     */
    void gc( bool fRecurse = true );

    /**
     * Adds a claim to this object.
     */
    void addClaim() throw() { ++m_refCount; }

    /**
     * Release a claim on this object.
     */
    void relClaim()
    {
        if( m_refCount == 0 ) throw new std::logic_error( "Too many relClaims" );
        --m_refCount;
    }

    /**
     * Write this object to a file or stream, in STL format.
     * @param out destination stream
     */
    virtual void saveSTL( std::ostream& out ) const {
        for_each_renderable_const( *this, it ) {
            (**it).saveSTL( out );
        }
    }

protected:
    /**
     * Render just this object.
     * @see Renderable::render
     */
    virtual void _render();

    virtual void _preRender();
    virtual void _postRender();

    virtual void _applyTransforms( bool fInverse = false );

    /**
     * Wrapper for glBegin to respect Options::Display::Wireframe.
     * @param mode Standard OpenGL drawing mode
     */
    void beginDraw( unsigned mode );

private:
    /* Direct descendants of this Renderable */
    RenderableList m_children;

    /* Used for garbage collection. */
    unsigned m_refCount;

    vec3 m_pos;
    vec3 m_scale;
    vec3 m_rot;  /* PYR */
};

/**
 * Generic container.
 * Simply renders descendants.
 */
class Group : public Renderable {
};

/**
 * 3D Vertex
 */
class Vertex : public vec3, public Renderable {
public:

    Vertex( nv_scalar x = 0.0, nv_scalar y = 0.0, nv_scalar z = 0.0 ) throw()
        : vec3( x, y, z ), Renderable()
    { };

    /**
     * @param v
     */
    explicit Vertex( vec3& v ) throw()
        : vec3( v ), Renderable()
    { };

    virtual ~Vertex() { };

    virtual void saveSTL( std::ostream& out ) const;

private:
    virtual void _render();
};

/**
 * Geometric face of arbitrary degree
 */
class Face : public Renderable {
public:

    Face() throw();

    Face( const Vertex* a, const Vertex* b, const Vertex* c, const Vertex* d = nullptr );

    /**
     * @param cFaces number of vertices in this face
     * @param ... Vertices to add
     */
    explicit Face( unsigned cVerts, ... );

    virtual ~Face() throw() { };

    const Vertex* getVertex( unsigned iVertex ) const
      { return dynamic_cast< const Vertex* >( getChild( iVertex ) ); }
    Vertex* getVertex( unsigned iVertex )
      { return dynamic_cast< Vertex* >( getChild( iVertex ) ); }

    /**
     * @return number of vertices in this Face
     */
    const unsigned num_vertices() const throw() { return children().size(); }

    /**
     * Calculate this Face's normal.
     * Uses only first three vertices; assumes planarity.
     * @return normal normal vector
     */
    const vec3 normal() const;

    virtual void saveSTL( std::ostream& out ) const;

private:
    virtual void _preRender();
    virtual void _postRender();
};

/**
 * Mesh
 */
class Mesh : public Renderable {
public:

    Mesh() throw()
        : Renderable()
    { };

    bool fAddFace( Face* f );

    bool fAddVertex( Vertex* v );

    const std::vector< Vertex* >& vertices() const { return m_vertices; }
          std::vector< Vertex* >& vertices()       { return m_vertices; }

    const std::vector< Face* >& faces() const { return m_faces; }
          std::vector< Face* >& faces()       { return m_faces; }

private:
    virtual void _render();

    std::vector< Face* > m_faces;
    std::vector< Vertex* > m_vertices;
};

/**
 * Plane
 */
class Plane : public Mesh {
public:

    /**
     * Creates a uniform planar patch
     * @param subdivisions number of squares per dimension
     * @param fCentered center the plane? Else, extend along +x/+y only.
     */
    Plane( unsigned subdivisions = 1, bool fCentered = true );
    virtual ~Plane();

    /**
     * Retrieve Vertex by index.
     * @example
     *    getVertexByIndex( 0, 0 ) == vertices()[0]
     * @param iX
     * @param iY
     */
    Vertex* getVertexByIndex( unsigned iX, unsigned iY );

private:
    unsigned m_subdivisions;
    unsigned m_n;
};

#endif
