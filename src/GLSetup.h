#ifndef _GL_SETUP_H_
#define _GL_SETUP_H_

#include <stdlib.h>
#include <string>
#include <GL/glut.h>

namespace GL {

    /**
    * Glut display callback
    */
    void display();

    void keyboard(unsigned char key, int x, int y);

    void specialKey( int key, int x, int y );

    void reshape(int w, int h);

    void passiveMotion(int x, int y);

    void drag(int x, int y);

    void mouse(int button, int state, int x, int y);

    void registerCallbacks();

    void setup();

    namespace Keys {
        typedef enum {
            ESC = 27,
            
            F3  = 3,
            F4  = 4,
            F9  = 9,

            KP_LEFT  = 100,
            KP_UP    = 101,
            KP_RIGHT = 102,
            KP_DOWN  = 103
        } Keycode;
    }
}

void saveSTL( const std::string& filename );

#endif
